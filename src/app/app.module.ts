import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';//NgModel esta aqui

import { AppComponent } from './app.component';
import {BandaDetalleComponent} from './banda-detalle.component';
import {BandasComponent } from './bandas.component';
import {BandaService } from './banda.service';
import {AppRoutingModule} from './app-routing.module';

import {RouterModule } from '@angular/router';

import {TableroComponent} from './tablero.component';//importar el componente tablero.
//import { Router, RouterOutlet } from "@angular/router";//Agregar *****

@NgModule({
  declarations: [
    AppComponent,
    BandaDetalleComponent,
    BandasComponent,
    TableroComponent //usar el componente tablero
  ],
  imports: [
    BrowserModule, 
    FormsModule,
    RouterModule.forRoot([
      {path:"bandas",component:BandasComponent},
      {path:"tablero",component:TableroComponent},
      {path:"",redirectTo:"/tablero",pathMatch:"full"},
      {path:"detalle/:id",component:BandaDetalleComponent}
    ])
  ],
  providers: [BandaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
